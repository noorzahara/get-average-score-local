/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igb.demoapp;

import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Reference;
import com.affymetrix.genometry.symmetry.SymWithProps;
import com.affymetrix.genometry.symmetry.impl.SeqSymmetry;
import java.util.HashMap;
import java.util.Map;
import org.lorainelab.igb.genoviz.extensions.SeqMapViewI;
import org.lorainelab.igb.services.IgbService;

/**
 *
 * @author ShamikaKulkarni
 */
@Component
public class ScoreCalculator {
    public static float score = 0;
    public static int count = 0;
    public static float avgscore = 0;
    public static IgbService igbService;
    Map<String, Object> props = new HashMap<>();
        
    @Reference
    public void setIgbService(IgbService igbService){
        this.igbService = igbService;
        System.out.println("igbservice : "+this.igbService.getSeqMapView());
    }
    
    public String calculateAvg(){
        score = 0;
        count = 0;
        String resp ="";
        SeqMapViewI seqMapView = igbService.getSeqMapView();
        for (SeqSymmetry sym : seqMapView.getSelectedSyms()) {
            if (sym == null) {
            props.clear();
            }

            if (sym instanceof SymWithProps) {
                props = ((SymWithProps) sym).getProperties();
                count++;
            }
       
            props.entrySet().forEach((entry) -> {
                    if ( entry.getKey().equals("score")){
                        score = score + Float.parseFloat(entry.getValue().toString());
                    }
            });     
        }
        
        if (count!=0){
                    avgscore = score/count;
                    resp = "Total Score = "+score+"\n"+"Count of selected syms = "+count+"\n"+"Average score = "+avgscore;
                } else {
                    resp = "No syms selected. Please select syms to calculate average score";
                }
        return resp;
    }  
}
